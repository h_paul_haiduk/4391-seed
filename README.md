# WTAMU Computer Science Programming Utility #



### What is this repository for? ###

* This program is a combination of two applications that are used in teaching students how to program and understand how programs work in a computing environment.
* Ver. 0.75.0

### How do I get set up? ###

* Visit this site to download the development environment used to create and edit this project: [NetBeans](https://netbeans.org/downloads/)  For most users, it is easier to download the version that contains the requisite version of the JDK.
* Visit this site for cloning the repository to your NetBeans installation: [Clone Instructions](https://netbeans.org/kb/docs/ide/git.html#clone)
* Requires Java 7 to compile
* Click Build to create the jar file

### What is in this version? ###

**Karel the Robot:**

* Function definitions
* All primitives: move, turnleft, pickbeeper, putbeeper, turnoff
* All checks for wall detection
* All checks for directionality
* All checks for beeper detection

**Machine Simulator:**

* All Pseudo OPs working
* All operations working
* Ability to print to a console via register F


### Future Changes ###

**Karel the Robot:**

* Ability to use the GUI in order to build the robot's world rather than only coding the world 

** Machine Simulator **

* Add disassembler
* Address screen resolution requirements that disenfranchise users with resolution challenged screens
* Address requests to move Assemble button to top for easier accessibility.
* Address issue where source editor fails to clear contents when opening a new file.

**Turing Machine**

* Requirements to be determined

### Who do I talk to? ###

* Email hhaiduk@wtamu.edu or brettdawson9@gmail.com for any questions