/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package karel.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import karel.controller.KarelController;
import karel.interpreter.InterpreterException;
import karel.world.parser.WorldParserException;

/**
 *
 * @author ladypomaster
 */
public class KarelView extends javax.swing.JFrame {

    public static final int CODE_TAB_INDEX = 0;
    public static final int WORLD_TAB_INDEX = 1;
    public static final Font DEFAULT_FONT = new Font("Tahoma", Font.PLAIN, 14);
    
    private final KarelController controller;
    private File sourceFile;
    private File worldFile;
    private int selectedTabIndex = CODE_TAB_INDEX;
    
    private ButtonGroup zoomButtonGroup = new ButtonGroup();
    private JFontChooser fontChooser = new JFontChooser();
    private Font selectedFont = DEFAULT_FONT;
    
    /**
     * Creates new form KarelView
     * @param controller
     */
    public KarelView(final KarelController controller) {
        
        this.controller = controller;
        
        WindowListener exitListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                controller.disposeKarelView();
            }
        };
        this.addWindowListener(exitListener);
        
        initComponents();
        
        startButton.setEnabled(false);
        stepButton.setEnabled(false);
        stopButton.setEnabled(false);
        resetButton.setEnabled(false);
     
        codeEditor.setTabbedPane(rightTabbedPane);
        worldEditor.setTabbedPane(rightTabbedPane);
        
        codeEditor.setTabIndex(CODE_TAB_INDEX);
        worldEditor.setTabIndex(WORLD_TAB_INDEX);
        
        codeEditor.setSaveMenuItem(saveMenuItem);
        worldEditor.setSaveMenuItem(saveMenuItem);
        
        createZoomButtonGroup();
        addLanguageReference();
        
        undoMenuItem.setAction(codeEditor.getUndoAction());
        redoMenuItem.setAction(codeEditor.getRedoAction());
        
        saveMenuItem.setEnabled(true);
        
        this.setSize(new Dimension(1000, 750));
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
    
    private void createZoomButtonGroup() {
        
        zoomButtonGroup.add(zoom100Button);
        zoomButtonGroup.add(zoom80Button);
        zoomButtonGroup.add(zoom60Button);
        
        ActionListener zoomListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == zoom100Button) {
                    world.setCellSize(50);
                }
                else if (e.getSource() == zoom80Button){
                    world.setCellSize(40);
                }
                else {
                    world.setCellSize(30);
                }
            }
            
        };
        
        zoom100Button.addActionListener(zoomListener);
        zoom80Button.addActionListener(zoomListener);
        zoom60Button.addActionListener(zoomListener);
    }
    
    private void addLanguageReference() {
        languageReferencePane.setContentType("text/html");
        InputStream inputStream =  KarelView.class.getResourceAsStream("/html/languageReference.html");
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            languageReferencePane.setText(sb.toString());
        } catch (IOException ex) {
            languageReferencePane.setText("Could not load language reference.");
        }
        try {
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(KarelView.class.getName()).log(Level.SEVERE, null, ex);
        }
        languageReferencePane.setEditable(false);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                languageReferenceScrollPane.getVerticalScrollBar().setValue(0);
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        leftToRightSplitPane = new javax.swing.JSplitPane();
        leftTabbedPane = new javax.swing.JTabbedPane();
        worldPanel = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        controlPanel = new javax.swing.JPanel();
        startButton = new javax.swing.JButton();
        stepButton = new javax.swing.JButton();
        stopButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();
        speedLabel = new javax.swing.JLabel();
        speedList = new javax.swing.JComboBox();
        loadButton = new javax.swing.JButton();
        world = new karel.world.World();
        languageReferenceScrollPane = new javax.swing.JScrollPane();
        languageReferencePane = new javax.swing.JEditorPane();
        rightTabbedPane = new javax.swing.JTabbedPane();
        codeEditor = new karel.view.TextEditor();
        worldEditor = new karel.view.TextEditor();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newSourceMenuItem = new javax.swing.JMenuItem();
        newWorldMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        openSourceFileMenuItem = new javax.swing.JMenuItem();
        openWorldFileMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        undoMenuItem = new javax.swing.JMenuItem();
        redoMenuItem = new javax.swing.JMenuItem();
        formatMenu = new javax.swing.JMenu();
        fontMenuItem = new javax.swing.JMenuItem();
        zoomMenu = new javax.swing.JMenu();
        zoom100Button = new javax.swing.JRadioButtonMenuItem();
        zoom80Button = new javax.swing.JRadioButtonMenuItem();
        zoom60Button = new javax.swing.JRadioButtonMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Karel the Robot");

        leftToRightSplitPane.setDividerLocation(650);
        leftToRightSplitPane.setResizeWeight(0.6);

        leftTabbedPane.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        worldPanel.setPreferredSize(new java.awt.Dimension(682, 792));

        statusLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        statusLabel.setText("Waiting for world input...");

        controlPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Control Panel", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("SansSerif", 1, 12))); // NOI18N
        controlPanel.setMinimumSize(new java.awt.Dimension(500, 50));
        controlPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 15, 5));

        startButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        startButton.setText("Start");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });
        controlPanel.add(startButton);

        stepButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stepButton.setText("Step");
        stepButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stepButtonActionPerformed(evt);
            }
        });
        controlPanel.add(stepButton);

        stopButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stopButton.setText("Stop");
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });
        controlPanel.add(stopButton);

        resetButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        resetButton.setText("Reset");
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });
        controlPanel.add(resetButton);

        speedLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        speedLabel.setText("Speed:");
        controlPanel.add(speedLabel);

        speedList.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        speedList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Instant", "1000%", "400%", "200%", "100%", "50%", "25%" }));
        speedList.setSelectedIndex(4);
        speedList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                speedListActionPerformed(evt);
            }
        });
        controlPanel.add(speedList);

        loadButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        loadButton.setText("Load World");
        loadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadButtonActionPerformed(evt);
            }
        });
        controlPanel.add(loadButton);

        world.setMinimumSize(new java.awt.Dimension(0, 0));
        world.setPreferredSize(new java.awt.Dimension(600, 600));

        javax.swing.GroupLayout worldPanelLayout = new javax.swing.GroupLayout(worldPanel);
        worldPanel.setLayout(worldPanelLayout);
        worldPanelLayout.setHorizontalGroup(
            worldPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(worldPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(worldPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(controlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(statusLabel))
                .addContainerGap(32, Short.MAX_VALUE))
            .addComponent(world, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        worldPanelLayout.setVerticalGroup(
            worldPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, worldPanelLayout.createSequentialGroup()
                .addComponent(world, javax.swing.GroupLayout.DEFAULT_SIZE, 618, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(controlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        leftTabbedPane.addTab("The World", worldPanel);

        languageReferenceScrollPane.setViewportView(languageReferencePane);

        leftTabbedPane.addTab("Language Reference", languageReferenceScrollPane);

        leftToRightSplitPane.setLeftComponent(leftTabbedPane);

        rightTabbedPane.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rightTabbedPane.setPreferredSize(new java.awt.Dimension(200, 792));
        rightTabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                rightTabbedPaneStateChanged(evt);
            }
        });

        codeEditor.setPreferredSize(new java.awt.Dimension(300, 700));
        rightTabbedPane.addTab("*Code editor", codeEditor);
        rightTabbedPane.addTab("*World editor", worldEditor);

        leftToRightSplitPane.setRightComponent(rightTabbedPane);

        getContentPane().add(leftToRightSplitPane, java.awt.BorderLayout.CENTER);

        fileMenu.setText("File");

        newSourceMenuItem.setText("New Source File...");
        newSourceMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newSourceMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(newSourceMenuItem);

        newWorldMenuItem.setText("New World File...");
        newWorldMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newWorldMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(newWorldMenuItem);
        fileMenu.add(jSeparator1);

        openSourceFileMenuItem.setText("Open Source File...");
        openSourceFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openSourceFileMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openSourceFileMenuItem);

        openWorldFileMenuItem.setText("Open World File...");
        openWorldFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openWorldFileMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openWorldFileMenuItem);
        fileMenu.add(jSeparator2);

        saveMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveMenuItem.setText("Save ");
        saveMenuItem.setEnabled(false);
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setText("Save as...");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);

        menuBar.add(fileMenu);

        editMenu.setText("Edit");

        undoMenuItem.setText("Undo...");
        editMenu.add(undoMenuItem);

        redoMenuItem.setText("Redo...");
        editMenu.add(redoMenuItem);

        menuBar.add(editMenu);

        formatMenu.setText("Format");

        fontMenuItem.setText("Font...");
        fontMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fontMenuItemActionPerformed(evt);
            }
        });
        formatMenu.add(fontMenuItem);

        menuBar.add(formatMenu);

        zoomMenu.setText("Zoom");

        zoom100Button.setSelected(true);
        zoom100Button.setText("100%");
        zoomMenu.add(zoom100Button);

        zoom80Button.setSelected(true);
        zoom80Button.setText("80%");
        zoomMenu.add(zoom80Button);

        zoom60Button.setSelected(true);
        zoom60Button.setText("60%");
        zoomMenu.add(zoom60Button);

        menuBar.add(zoomMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        if (startButton.getText().equals("Start")) {

            try {
                controller.startSimulation(codeEditor.getText());
            } catch (InterpreterException ex) {
                String message = ex.getMessage();
                codeEditor.highlightLine(ex.getLineNum(), TextEditor.ERROR_COLOR);
                setStatusMessage(message);
                showDialog(message);
                rightTabbedPane.setEnabled(true);
                codeEditor.setEditable(true);
            }
        }
        else {
            controller.runSimulation();
        }
    }//GEN-LAST:event_startButtonActionPerformed

    private void stepButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stepButtonActionPerformed
         try {
            controller.stepSimulation(codeEditor.getText());
        } catch (InterpreterException ex) {
            String message = ex.getMessage();
            codeEditor.highlightLine(ex.getLineNum(), TextEditor.ERROR_COLOR);
            setStatusMessage(message);
            showDialog(message);
            rightTabbedPane.setEnabled(true);
            codeEditor.setEditable(true);
        }
    }//GEN-LAST:event_stepButtonActionPerformed

    private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopButtonActionPerformed
        controller.stopSimulation();
    }//GEN-LAST:event_stopButtonActionPerformed

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        controller.resetSimulation();
    }//GEN-LAST:event_resetButtonActionPerformed

    private void speedListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_speedListActionPerformed
        controller.setSimulationSpeed(getSpeed());
    }//GEN-LAST:event_speedListActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        JFileChooser fc = new JFileChooser();
        int returnVal;
        String content;
        String tabTitle = rightTabbedPane.getTitleAt(selectedTabIndex);
        
        if (selectedTabIndex == CODE_TAB_INDEX) {
            content = codeEditor.getText();
            if (sourceFile != null) {
                controller.saveFile(sourceFile, content);
                rightTabbedPane.setTitleAt(selectedTabIndex, tabTitle.replace("*", ""));
                saveMenuItem.setEnabled(false);
            }
            else {
                
                FileFilter karelFilter = new FileNameExtensionFilter("Karel source (.karel)","karel");
                fc.addChoosableFileFilter(karelFilter);
                fc.setFileFilter(karelFilter);
                fc.setSelectedFile(new File("untitled.karel"));
                returnVal = fc.showSaveDialog(this);
                
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    sourceFile = fc.getSelectedFile();
                    controller.saveFile(sourceFile, content);
                    rightTabbedPane.setTitleAt(selectedTabIndex, tabTitle.replace("*", ""));
                    saveMenuItem.setEnabled(false);
                }
            }
        }
        else {
            content = worldEditor.getText();
            if (worldFile != null) {
                controller.saveFile(worldFile, content);
                rightTabbedPane.setTitleAt(selectedTabIndex, tabTitle.replace("*", ""));
                saveMenuItem.setEnabled(false);
            }
            else {
                FileFilter worldFilter = new FileNameExtensionFilter("Karel world (.world)","world");
                fc.addChoosableFileFilter(worldFilter);
                fc.setFileFilter(worldFilter);
                fc.setSelectedFile(new File("untitled.world"));
                returnVal = fc.showSaveDialog(this);
                
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    if (!(fc.getSelectedFile().getName().contains(".")) || !fc.getSelectedFile().getName().substring(fc.getSelectedFile().getName().lastIndexOf(".")).equals(".world")){
                    String oldName = fc.getSelectedFile().getAbsolutePath() + ".world";
                    fc.setSelectedFile(new File(oldName));
                }
                    worldFile = fc.getSelectedFile();
                    controller.saveFile(worldFile, content);
                    rightTabbedPane.setTitleAt(selectedTabIndex, tabTitle.replace("*", ""));
                    saveMenuItem.setEnabled(false);
                }
            }
        }
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void openSourceFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openSourceFileMenuItemActionPerformed
        JFileChooser fc = new JFileChooser();
        FileFilter karelFilter = new FileNameExtensionFilter("Karel source (.karel)","karel");
        fc.addChoosableFileFilter(karelFilter);
        fc.setFileFilter(karelFilter);
        int returnVal = fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            if (!(fc.getSelectedFile().getName().contains(".")) || !fc.getSelectedFile().getName().substring(fc.getSelectedFile().getName().lastIndexOf(".")).equals(".karel")){
                    String oldName = fc.getSelectedFile().getAbsolutePath() + ".karel";
                    fc.setSelectedFile(new File(oldName));
                }
            sourceFile = fc.getSelectedFile();
            selectedTabIndex = CODE_TAB_INDEX;
            TextEditor textEditor = (TextEditor)rightTabbedPane.getComponent(selectedTabIndex);
            textEditor.setText(controller.openFile(sourceFile));
            rightTabbedPane.setSelectedIndex(selectedTabIndex);
        }
    }//GEN-LAST:event_openSourceFileMenuItemActionPerformed

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuItemActionPerformed
        
        JFileChooser fc = new JFileChooser();
        selectedTabIndex = rightTabbedPane.getSelectedIndex();
        int returnVal;
        
         if (selectedTabIndex == CODE_TAB_INDEX) {
             
            FileFilter karelFilter = new FileNameExtensionFilter("Karel source (.karel)","karel");
            fc.addChoosableFileFilter(karelFilter);
            fc.setFileFilter(karelFilter);
            fc.setSelectedFile(new File("untitled.karel"));
            returnVal = fc.showSaveDialog(this);
            
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                if (!(fc.getSelectedFile().getName().contains(".")) || !fc.getSelectedFile().getName().substring(fc.getSelectedFile().getName().lastIndexOf(".")).equals(".karel")){
                    String oldName = fc.getSelectedFile().getAbsolutePath() + ".karel";
                    fc.setSelectedFile(new File(oldName));
                }
                sourceFile = fc.getSelectedFile();
                controller.saveFile(sourceFile, codeEditor.getText());
                saveMenuItem.setEnabled(false);
                String tabTitle = rightTabbedPane.getTitleAt(selectedTabIndex);
                rightTabbedPane.setTitleAt(selectedTabIndex, tabTitle.replace("*", ""));
            }
        }
        else {
            FileFilter worldFilter = new FileNameExtensionFilter("Karel world (.world)","world");
            fc.addChoosableFileFilter(worldFilter);
            fc.setFileFilter(worldFilter);
            fc.setSelectedFile(new File("untitled.world"));
            returnVal = fc.showSaveDialog(this);
            
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                if (!(fc.getSelectedFile().getName().contains(".")) || !fc.getSelectedFile().getName().substring(fc.getSelectedFile().getName().lastIndexOf(".")).equals(".world")){
                    String oldName = fc.getSelectedFile().getAbsolutePath() + ".world";
                    fc.setSelectedFile(new File(oldName));
                }
                worldFile = fc.getSelectedFile();
                controller.saveFile(worldFile, worldEditor.getText());
                saveMenuItem.setEnabled(false);
                String tabTitle = rightTabbedPane.getTitleAt(selectedTabIndex);
                rightTabbedPane.setTitleAt(selectedTabIndex, tabTitle.replace("*", ""));
            }
        }
    }//GEN-LAST:event_saveAsMenuItemActionPerformed

    private void openWorldFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openWorldFileMenuItemActionPerformed
        JFileChooser fc = new JFileChooser();
        FileFilter worldFilter = new FileNameExtensionFilter("Karel world (.world)","world");
        fc.addChoosableFileFilter(worldFilter);
        fc.setFileFilter(worldFilter);
        int returnVal = fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            worldFile = fc.getSelectedFile();
            selectedTabIndex = WORLD_TAB_INDEX;
            TextEditor textEditor = (TextEditor)rightTabbedPane.getComponent(selectedTabIndex);
            textEditor.setText(controller.openFile(worldFile));
            rightTabbedPane.setSelectedIndex(selectedTabIndex);
        }
    }//GEN-LAST:event_openWorldFileMenuItemActionPerformed

    private void loadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadButtonActionPerformed
        loadWorld();
    }//GEN-LAST:event_loadButtonActionPerformed

    /** Switches the save and undo/redo context to the selected editor.
     * @param evt 
     */
    private void rightTabbedPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_rightTabbedPaneStateChanged

        selectedTabIndex = rightTabbedPane.getSelectedIndex();
        String tabTitle = rightTabbedPane.getTitleAt(selectedTabIndex);
        if (tabTitle.startsWith("*")) {
            saveMenuItem.setEnabled(true);
        }
        else {
            saveMenuItem.setEnabled(false);
        }
        if (selectedTabIndex == CODE_TAB_INDEX) {
            undoMenuItem.setAction(codeEditor.getUndoAction());
            redoMenuItem.setAction(codeEditor.getRedoAction());
        }
        else {
            undoMenuItem.setAction(worldEditor.getUndoAction());
            redoMenuItem.setAction(worldEditor.getRedoAction());
        }
        
    }//GEN-LAST:event_rightTabbedPaneStateChanged

    private void fontMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fontMenuItemActionPerformed

        fontChooser.setSelectedFont(selectedFont);
        int value = fontChooser.showDialog(this);
        
        if (value == JFontChooser.OK_OPTION) {
            selectedFont = fontChooser.getSelectedFont();
            worldEditor.setTextPaneFont(selectedFont);
            codeEditor.setTextPaneFont(selectedFont);
        }
    }//GEN-LAST:event_fontMenuItemActionPerformed

    private void newSourceMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newSourceMenuItemActionPerformed
        int option = JOptionPane.showConfirmDialog(
                this, "Are you sure you want to create a new source file?");
        
        if (option == JOptionPane.YES_OPTION) {
            rightTabbedPane.setSelectedIndex(CODE_TAB_INDEX);
            sourceFile = null;
            codeEditor.setText("");
            rightTabbedPane.setTitleAt(CODE_TAB_INDEX, "*Code editor");
            saveMenuItem.setEnabled(true);
        }
    }//GEN-LAST:event_newSourceMenuItemActionPerformed

    private void newWorldMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newWorldMenuItemActionPerformed
        int option = JOptionPane.showConfirmDialog(
                this, "Are you sure you want to create a new world file?");
        
        if (option == JOptionPane.YES_OPTION) {
            rightTabbedPane.setSelectedIndex(WORLD_TAB_INDEX);
            worldFile = null;
            worldEditor.setText("");
            rightTabbedPane.setTitleAt(WORLD_TAB_INDEX, "*World editor");
            saveMenuItem.setEnabled(true);
        }
    }//GEN-LAST:event_newWorldMenuItemActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private karel.view.TextEditor codeEditor;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuItem fontMenuItem;
    private javax.swing.JMenu formatMenu;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JEditorPane languageReferencePane;
    private javax.swing.JScrollPane languageReferenceScrollPane;
    private javax.swing.JTabbedPane leftTabbedPane;
    private javax.swing.JSplitPane leftToRightSplitPane;
    private javax.swing.JButton loadButton;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem newSourceMenuItem;
    private javax.swing.JMenuItem newWorldMenuItem;
    private javax.swing.JMenuItem openSourceFileMenuItem;
    private javax.swing.JMenuItem openWorldFileMenuItem;
    private javax.swing.JMenuItem redoMenuItem;
    private javax.swing.JButton resetButton;
    private javax.swing.JTabbedPane rightTabbedPane;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JLabel speedLabel;
    private javax.swing.JComboBox speedList;
    private javax.swing.JButton startButton;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JButton stepButton;
    private javax.swing.JButton stopButton;
    private javax.swing.JMenuItem undoMenuItem;
    private karel.world.World world;
    private karel.view.TextEditor worldEditor;
    private javax.swing.JPanel worldPanel;
    private javax.swing.JRadioButtonMenuItem zoom100Button;
    private javax.swing.JRadioButtonMenuItem zoom60Button;
    private javax.swing.JRadioButtonMenuItem zoom80Button;
    private javax.swing.JMenu zoomMenu;
    // End of variables declaration//GEN-END:variables
    public void setStatusMessage(String msg) {
        statusLabel.setText("<html>" + msg.replaceAll("\n", "<br>") + "</html>");
    }

    public void disableStopButton() {
        stopButton.setEnabled(false);
    }

    public void disableStepButton() {
        stepButton.setEnabled(false);
    }

    public void disableStartButton() {
        startButton.setEnabled(false);
    }

    public void showDialog(final String msg) {

        // create JOptionPane on Event Dispatch Thread
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                    JOptionPane.showMessageDialog(KarelView.this, msg);
            }
        });
    }
    
    public void highlightLine(int tabIndex, int lineNumber, Color color) {
        if (tabIndex == CODE_TAB_INDEX) {
            codeEditor.highlightLine(lineNumber, color);
        }
        else {
            worldEditor.highlightLine(lineNumber, color);
        }
    }
    
    public void setStartState() {
        startButton.setText("Run");
        
        setRunState();
    }
    
    public void setRunState() {
        rightTabbedPane.setEnabled(false);
        codeEditor.setEditable(false);
        startButton.setEnabled(false);
        stepButton.setEnabled(false);
        stopButton.setEnabled(true);
        resetButton.setEnabled(true);
        loadButton.setEnabled(false);
        fileMenu.setEnabled(false);
        editMenu.setEnabled(false);
        
        rightTabbedPane.setSelectedComponent(codeEditor);
        selectedTabIndex = CODE_TAB_INDEX;
    }
    
    public void setStepState() {
        rightTabbedPane.setEnabled(false);
        codeEditor.setEditable(false);
        startButton.setText("Run");
        stopButton.setEnabled(false);
        resetButton.setEnabled(true);
        loadButton.setEnabled(false);
        fileMenu.setEnabled(false);
        editMenu.setEnabled(false);
        
        rightTabbedPane.setSelectedComponent(codeEditor);
        selectedTabIndex = CODE_TAB_INDEX;
    }
    
    public void setStoppedState() {
        startButton.setEnabled(true);
        stepButton.setEnabled(true);
        stopButton.setEnabled(false);
    }

    /** Resets all controls to their starting properties. */
    public void reset() {
            
        loadWorld();
        
        rightTabbedPane.setEnabled(true);
        codeEditor.setEditable(true);
        
        codeEditor.resetHighlighter();
        worldEditor.resetHighlighter();

        startButton.setText("Start");
        startButton.setEnabled(true);
        stepButton.setEnabled(true);
        stopButton.setEnabled(false);
        resetButton.setEnabled(false);
        loadButton.setEnabled(true);
        fileMenu.setEnabled(true);
        editMenu.setEnabled(true);
    }

    public int getSpeed() {

        switch((String) speedList.getSelectedItem()) {
        case "Instant":
                return 0;
        case "1000%":
                return 15;
        case "400%":
                return 37;
        case "200%":
                return 75;
        case "100%":
                return 150;
        case "50%":
                return 300;
        case "25%":
                return 600;
        default:
                return 150;
        }
    }
    
    public karel.world.World getWorld() {
        return world;
    }
    
    private void loadWorld() {
        try {
            controller.loadWorld(world, worldEditor.getText());
            startButton.setEnabled(true);
            stepButton.setEnabled(true);
            setStatusMessage(world.getRobotStatus() + "\nInstruction count: 0"
                    + "\nConditional count: 0");
        } catch (WorldParserException ex) {
            String message = ex.getMessage();
            if (ex.getLineNum() != -1) {
                worldEditor.highlightLine(ex.getLineNum(), TextEditor.ERROR_COLOR);
            }
            setStatusMessage(message);
            showDialog(message);
            rightTabbedPane.setSelectedIndex(WORLD_TAB_INDEX);
            startButton.setEnabled(false);
            stepButton.setEnabled(false);
        }
    }
}
