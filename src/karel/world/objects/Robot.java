/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package karel.world.objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import karel.world.BeeperException;
import karel.world.RobotMoveException;

/**
 *
 * @author Ryan Ball
 */
public class Robot extends Path2D.Double{
    
    Shape robot;
    private Rooms<Integer,Integer,Room> rooms;
    private int numberOfBeepers;
    private int row;
    private int col;
    private Direction dir;
    private Color robotColor = Color.blue;
    
    public Robot(int row, int col, Direction dir, int numberOfBeepers, 
            Rooms<Integer,Integer,Room> rooms) {
        
        this.numberOfBeepers = numberOfBeepers;
        this.rooms = rooms;
        this.row = row;
        this.col = col;
        this.dir = dir;
    }
    
    public void draw(Graphics2D g2) {
        g2.setColor(robotColor);
        g2.draw(this);
    }
    
    public void draw(Graphics2D g2, int x, int y, double moveOffset, double turnOffset, int cellSize) {
        
        g2.setColor(robotColor);
        if (turnOffset == 0) {
            switch(dir) {
                case NORTH:
                    reset();
                    moveTo(x + cellSize/2.0, y - cellSize * 0.85 - moveOffset);
                    lineTo(x + cellSize/5.0, y - cellSize * 0.15 - moveOffset);
                    lineTo(x + cellSize * 4.0/5.0, y - cellSize * 0.15 - moveOffset);
                    closePath();   
                    break;
                case SOUTH:
                    reset();
                    moveTo(x + cellSize/2.0, y - cellSize * 0.15 + moveOffset);
                    lineTo(x + cellSize/5.0, y - cellSize * 0.85 + moveOffset);
                    lineTo(x + cellSize * 4.0/5.0, y - cellSize * 0.85 + moveOffset);
                    closePath();
                    break;
                case EAST:
                    reset();
                    moveTo(x + cellSize * 0.85 + moveOffset, y - cellSize/2.0);
                    lineTo(x + cellSize * 0.15 + moveOffset, y - cellSize/5.0);
                    lineTo(x + cellSize * 0.15 + moveOffset, y - cellSize * 4.0/5.0);
                    closePath();
                    break;
                case WEST:
                    reset();
                    moveTo(x + cellSize * 0.15 - moveOffset, y - cellSize/2.0);
                    lineTo(x + cellSize * 0.85 - moveOffset, y - cellSize/5.0);
                    lineTo(x + cellSize * 0.85 - moveOffset, y - cellSize * 4.0/5.0);
                    closePath();
            }
            g2.draw(this);
        }
        else {
            AffineTransform transform = AffineTransform.getRotateInstance(
                    -Math.toRadians(turnOffset), x + cellSize/2.0,
                     y - cellSize/2.0);
            Shape robotShape = this.createTransformedShape(transform);
            g2.draw(robotShape);
        }
    }
    
    public int getRow() {
        return row;
    }
    
    public int getCol() {
        return col;
    }
    
    public int getNumberOfBeepers() {
        return numberOfBeepers;
    }
    
    public Direction getDirection() {
        return dir;
    }
    
    public Direction getLeft() {
        switch(dir) {
            case NORTH:
                return Direction.WEST;
            case SOUTH:
                return Direction.EAST;
            case EAST:
                return Direction.NORTH;
            case WEST:
                return Direction.SOUTH;
            default:
                return null;
        }
    }
    
    public Direction getRight() {
        switch(dir) {
            case NORTH:
                return Direction.EAST;
            case SOUTH:
                return Direction.WEST;
            case EAST:
                return Direction.SOUTH;
            case WEST:
                return Direction.NORTH;
            default:
                return null;
        }
    }
    
    public void putBeeper() throws BeeperException {
        if (numberOfBeepers == 0 ) {
            throw new BeeperException("Robot is out of beepers and is turning off.");
        }
        else {
            numberOfBeepers--;
        }
    }
    
    public void pickBeeper() {
        numberOfBeepers++;
    }
    
    public void turnleft() {
        switch(dir) {
            case NORTH:
                dir = Direction.WEST;
                break;
            case SOUTH:
                 dir = Direction.EAST;
                break;
            case WEST:
                 dir = Direction.SOUTH;
                break;
            case EAST:
                 dir = Direction.NORTH;
                break; 
        }
    }
    
    public String getStatus() {
        Room room = rooms.get(row, col);
        int roomBeepers = 0;
        
        if (room != null) {
            roomBeepers = room.getNumOfBeepers();
        }
        
        return String.format("Robot is at position [%d,%d], facing %s, carrying " +
                "%d beepers, and on top of %d beepers.", col, row, dir.toString().toLowerCase(),
                numberOfBeepers, roomBeepers);
    }
    
    public void move(Direction dir) {
        switch(dir) {
            case NORTH:
                row++;
                break;
            case SOUTH:
                row--;
                break;
            case WEST:
                col--;
                break;
            case EAST:
                col++;
                break;
        }
    }
}
